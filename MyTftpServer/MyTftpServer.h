/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      18th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#ifndef MYTFTPSERVER_H
#define MYTFTPSERVER_H

#include "Arguments.h"

#include <vector>

#include <sys/socket.h>
#include <arpa/inet.h>


/*
 * Operation codes (defined in IEN 133)
 */
#define RRQ		1
#define WRQ		2
#define DATA	3
#define ACK		4
#define ERROR	5
#define OACK    6

/*
 * Error codes for use in error packets (defined in IEN 133 and RFC 1350)
 */
#define ERR_UNDEFINED				0
#define ERR_FILE_NOT_FOUND			1
#define ERR_ACCESS_VIOLATION		2
#define ERR_DISC_FULL				3
#define ERR_ILLEGAL_OP				4
#define ERR_UNKNOWN_ID				5
#define ERR_FILE_ALREADY_EXISTS		6

/*
 * Supported tranfers modes
 */
#define	OCTET		"octet"
#define	NETASCII	"netascii"

/*
 * Supported options to negotiate
 */
#define	BLKSIZE   	"blksize"
#define	TIMEOUT 	"timeout"
#define	TSIZE 		"tsize"
#define MULTICAST 	"multicast"

/*
 * Retun codes of some methods
 */

#define RQ_ERR		0
#define RQ_OK		1

/*
 * Some default limits
 */
#define DATA_MAXLENGTH		512
#define CONTENTS_MAXLENGTH	510
#define FILENAME_MAXLENGTH 	255

/*
 * Packet for communication
 */
struct packetComm {
    signed short int opcode;
    char contents[CONTENTS_MAXLENGTH];
}__attribute__((packed));

/*
 * Block for data or error
 */
struct packetData {
    signed short int opcode;
    signed short int block;
    char data[DATA_MAXLENGTH];
}__attribute__((packed));


/*
 * Main class of whole project.
 */
class MyTftpServer
{
    private:

    	/*
    	 * Processed arguments of command line
    	 */
        Arguments* arguments;

        /*
    	 * Separated contents of request packet
    	 */
        std::vector<char*> packetContents;

        /*
    	 * Holds processed arguments of command line
    	 */
        std::string file;

        /*
    	 * Info about client requests
    	 */
        char stringIP[INET6_ADDRSTRLEN];
        std::string mode;
        bool extended, multicast;

        struct addrinfo *result;

        /*
    	 * Structures for packet data
    	 */
        struct packetData *error;
        struct packetComm *request;

        /*
    	 * PSettings used for tranfer
    	 */
        unsigned short timeout, blksize;
        unsigned long long tsize;

        /*
    	 * Flags of need to ack options to client
    	 */
        bool setTimeout, setBlksize, setTsize;

        struct sockaddr_storage clientAddr;
        unsigned int addrLen;

        /*
		 * Starts listening service on specified interfaces and ports
		 */
        void startServices();   

        void listen(int socket);

        /*
		 * Processes contents of request packet
		 */
        int processPacketContents(char raw[CONTENTS_MAXLENGTH]);

        /*
		 * Parses contents of request packet
		 */
        int parsePacketContents(char raw[CONTENTS_MAXLENGTH]);

        /*
		 * Sets server settings and responses packet contents according to values from request packet
		 */
        int usePacketContents();

        /*
		 * Validates filename (lenght and presence of slash).
		 *
		 * Returns nonzero values on success.
		 */
        int validateFileName();

        /*
		 * Returns bool value about existence of file specified by working directory and filename
		 */
        bool fileExists ();

        /*
		 * Returns bool value about readability of file specified by working directory and filename
		 */
        bool fileReadable ();

        size_t getFilesize();

        void send(int clientSocket);
        void recieve(int clientSocket);

    public:
    	
        /*
         * Constructor of server, paramaters are arguments of command line (at least one, 
         * working directory, is always required.)
         */
        MyTftpServer(int argc, char** argv);

        ~MyTftpServer();

        /*
		 * Either calls Print Help method, or starts calls method that starts service on desired 
		 * interfaces & ports.
		 */
        void run();
};

#endif

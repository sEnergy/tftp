################################################################################
 #
 # Project name:        TFTP server
 # Author:              Marcel Fiala
 # E-mail:              xfiala47@stud.fit.vutbr.cz
 # Date (created):      18th November 2014
 # Encoding:            UTF-8
 #
################################################################################

NAME=MyTftpServer
CC=g++ 
CFLAGS=-std=c++0x -Wall -Wextra -Werror -pedantic

SRC_FILES = $(wildcard *.cpp)
HEADER_FILES = $(wildcard *.h)
OBJ_FILES = $(patsubst %.cpp, %.o, $(SRC_FILES))

VGPARAMS=--tool=memcheck --leak-check=full --leak-resolution=high -v \
		--show-reachable=yes --undef-value-errors=yes --track-origins=yes \
		--show-possibly-lost=yes

.PHONY: link debug ddd gdb dep clean run grind stats release

# creation of object files
%.o : %.cpp
	$(CC) $(CFLAGS) -c $<

# start rule
all: dep link
	@rm -r dependencies.list

# linking object files in one executable file
link: $(NAME)

# creating dependencies list
dep:
	$(CC) -MM *.cpp > dependencies.list

# include of created dependencies
-include dependencies.list

# linking object files in one executable file
$(NAME): $(OBJ_FILES)
	$(CC) $(CFLAGS) $(OBJ_FILES) -o $@

# valgrind test
grind:
	@valgrind $(VGPARAMS) ./$(NAME) -d path -t 77 -s 1000 -a 127.0.0.1,-14

# cleaning unnecessary files
clean:
	@rm -r $(OBJ_FILES) $(NAME)

# standard run
run:
	./$(NAME) -d path -t 77 -s 1000 -a 127.0.0.1,11111

# standard run
runsu:
	sudo ./$(NAME) -d path -t 77 -s 50 -a 127.0.0.1,69

# compilation for debugging
debug: CFLAGS+=-g3 -ggdb
debug: dep link

# compilation for release
release: CFLAGS+=-O2
release: dep link

# debugging with DDD
ddd:
	ddd ./$(NAME)

# debugging with GDB
gdb:
	gdb ./$(NAME)

# showing project stats
stats:
	@echo -n "Projects stats\n--------------"
	@echo -n "\nTotal files:  " && find ./*.h ./*.cpp -type f -printf x | wc -c
	@echo -n "Source files: " && find ./*.cpp -type f -printf x | wc -c
	@echo -n "Header files: " && find ./*.h -type f -printf x | wc -c
	@echo -n "\nLines of code total: " && wc -l $(SRC_FILES) $(HEADER_FILES) \
		| tail -n 1 | sed -r "s/[ ]*([0-9]+).*/\1/g"
	@echo -n "   -in source files: " && wc -l $(SRC_FILES) \
		| tail -n 1 | sed -r "s/[ ]*([0-9]+).*/\1/g"
	@echo -n "   -in header files: " && wc -l $(HEADER_FILES) \
		| tail -n 1 | sed -r "s/[ ]*([0-9]+).*/\1/g"
	@echo -n "\nTotal size of project: " && du -hsc $(SRC_FILES) $(HEADER_FILES) \
		| tail -n 1 | cut -f 1
	@echo -n "Size of source files:  " && du -hsc $(SRC_FILES) \
		| tail -n 1 | cut -f 1
	@echo -n "Size of header files:  " && du -hsc $(HEADER_FILES) \
		| tail -n 1 | cut -f 1

# End of Makefile #

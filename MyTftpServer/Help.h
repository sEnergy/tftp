/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      18th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#ifndef HELP_H
#define HELP_H

/*
 * Just a simple class only for printing help message.
 */
class Help
{
    public:
        static void PrintHelpMessage (void);
};

#endif
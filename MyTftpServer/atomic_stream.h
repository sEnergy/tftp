/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      27th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#define add_time_to_stream {													\
								time_t tm = time(NULL);							\
	        					struct tm *t = localtime (&tm);					\
	        																	\
	        					std::string mon = std::to_string(t->tm_mon);		\
	        					if (mon.size() == 1) mon = "0" + mon;			\
																				\
	        					std::string mday = std::to_string(t->tm_mday);	\
	        					if (mday.size() == 1) mday = "0" + mday;		\
																				\
	        					std::string hour = std::to_string(t->tm_hour);	\
	        					if (hour.size() == 1) hour = "0" + hour;		\
																				\
	        					std::string min = std::to_string(t->tm_min);	\
	        					if (min.size() == 1) min = "0" + min;			\
	        																	\
	        					std::string sec = std::to_string(t->tm_sec);	\
	        					if (sec.size() == 1) sec = "0" + sec;			\
	        																	\
								stream  										\
									<< "[" << t->tm_year+1900 << "-" 			\
									<< mon << "-" << mday << " "				\
									<< hour << ":" << min << ":"				\
									<< sec << "] ";								\
							}
  										


#define cout(msg) { 												\
							std::stringstream stream; 					\
							add_time_to_stream  						\
							stream msg;  								\
							std::cout << stream.str() << std::flush;	\
						 }


#define cerr(msg) { 												\
							std::stringstream stream; 					\
							add_time_to_stream  						\
							stream msg;  								\
							std::cerr << stream.str() << std::flush;	\
						 }

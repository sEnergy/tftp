/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      18th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <string>
#include <vector>

#include <string>

/*
 * Limits for program arguments
 */
#define MINIMUM_ARGUMENTS 3
#define MAXIMUM_ARGUMENTS 9

#define UNSET -1

/*
 * Delimiters used for parsing
 */
#define ADDRESS_DELIMITER '#'
#define ADDR_PORT_DELIMITER ','

/*
 * Defult settings for transfer
 */
#define DEFAULT_TIMEOUT             1
#define DEFAULT_ACCEPTED_TIMEOUT    3

/*
 * Limits of blocksize, defined in RFC 2348
 */
#define BLOCK_MINSIZE   8
#define BLOCK_MAXSIZE   65464

/*
 * Default address and port
 */
#define LOCALHOST "127.0.0.1"
#define DEFAULT_PORT "69"

/*
 * Class for arguments of program and their processing.
 */
class Arguments
{
    private:
        
        std::string directory; // working directory
        int timeout; // timeout limit (for retransmission)
        int size; // max block size

        std::vector<std::string> addresses; // listened addresses
        std::vector<std::string> ports; // listened port (one for each address)

        bool onlyHelp; // user only wants to read help text

        /*
         * Process arguments of command line - parsing setup values for TFTP server.
         */
        void processArguments(int argc, char** argv);

        /*
         * Parse address-port pairs from address argument. If none is specified, 
         * default address is 127.0.0.1 and default port is 69.
         */
        void processAddresses(std::string in);

        /*
         * Validates working directory (existence only!)
         */
        void validateWorkingDirectory();

    public:

        Arguments(int argc, char** argv);
        ~Arguments(void);

        /*
         * Prints info about TFTP server setup before it real start.
         */
        void printArgumentsInfo();

        /*
         * Getter for working directory.
         */
        std::string getDirectory();

        /*
         * Getter for retransmission timeout time.
         */
        unsigned int getTimeout();

        /*
         * Getter for block size value.
         */
        unsigned int getSize();

        /*
         * Getter for info if user only want help.
         */
        bool getOnlyHelp();

        /*
         * Getter for specified address
         */
        std::string getAddress(unsigned int i);

        /*
         * Getter for specified address
         */
        std::string getPort(unsigned int i);

        /*
         * Getter for number of user pairs interface,port
         */
        unsigned int getInterfaceCount();
};

#endif
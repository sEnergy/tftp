/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      22nd November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#ifndef ERROR_H
#define ERROR_H

/*
 * Not enough arguments of command line (atd last working directory must be specified).
 */
#define ERROR_NOT_ENOUGH_ARGUMENTS "Not enough arguments."

/*
 * Except for occurence of "-h" or "--help" argument only, the number of command line
 * arguments must be even.
 */
#define ERROR_ILLEGAL_ARGC "Illegal number of command line arguments."

/*
 * Only one instance of each argument type is legal.
 */
#define ERROR_DUPLICATE_ARGUMENT "Dumplicate argument is unallowed."

/*
 * Invalid value of "size" argument("-s")
 */
#define ERROR_INVALID_SIZE "Invalid size argument."

/*
 * Invalid value of "timeout" argument("-t")
 */
#define ERROR_INVALID_TIMEOUT "Invalid timeout argument (must be 1 or higher)."

/*
 * Unknown argument used
 */
#define ERROR_INVALID_ARGUMENT "Unknown argument."

/*
 * Working directory is required argument.
 */
#define ERROR_MISSING_DIRECTORY "Missing working directory path argument."

/*
 * Working directory does not exist.
 */
#define ERROR_EXIST_DIRECTORY "Specified directory doest not exist."

/*
 * Error during creating datagram.
 */
#define ERROR_DATAGRAM_SOCKET "Could not create datagram socket."

/*
 * Error ruding bind socket.
 */
#define ERROR_BIND "Will not listen on "

/*
 * Error during recv() call.
 */
#define ERROR_RECV "Error during recieving request packet from client - code "

/*
 * Error during address&port resolving
 */
#define ERR_GETADDR "Error getting info about "

#endif

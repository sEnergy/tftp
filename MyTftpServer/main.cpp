/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      18th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/
 
#include "MyTftpServer.h"
#include "Help.h"

#include <cstdlib>
#include <iostream>

int main (int argc, char** argv)
{
    std::cout << std::endl;

    try {
        MyTftpServer server(argc, argv);
        server.run();
    } catch (const std::exception& e) {
        std::cerr << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
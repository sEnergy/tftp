/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      18th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#include "Arguments.h"
#include "Error.h"

#include "atomic_stream.h"

#include <stdexcept>
#include <iostream>
#include <cstring>
#include <string>
#include <sstream>
#include <sys/stat.h>

Arguments::Arguments(int argc, char** argv)
{
    this->processArguments(argc, argv);
}

Arguments::~Arguments(void)
{
    this->addresses.clear();
    this->ports.clear();
}

/*
 * Process arguments of command line - parsing setup values for TFTP server.
 */
void Arguments::processArguments (int argc, char** argv)
{
    std::string address("");

    this->directory = "";
    this->onlyHelp = false;
    this->size = this->timeout = UNSET;

    // chceck if user only wants help
    if (argc == 2 && (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")))
    {
        this->onlyHelp = true;
    }

    // to few arguments
    if (argc < MINIMUM_ARGUMENTS)
    {
        throw std::logic_error(ERROR_NOT_ENOUGH_ARGUMENTS);
    }

    // to much or odd number of arguments
    if (!(argc%2) || argc > MAXIMUM_ARGUMENTS)
    {
        throw std::logic_error(ERROR_ILLEGAL_ARGC);
    }

    // if number of args is correct, process them
    for (int i = 1; i < argc-1; i = i+2)
    {
        if (!strcmp(argv[i], "-d")) // working directory
        {
            if (!this->directory.size())
            {
                this->directory = argv[i+1];

            }
            else
            {
                throw std::logic_error(ERROR_DUPLICATE_ARGUMENT);
            }
        }
        else if (!strcmp(argv[i], "-a")) // listened addresses
        {
            if (!address.length())
            {
                address = argv[i+1];
            }
            else
            {
                throw std::logic_error(ERROR_DUPLICATE_ARGUMENT);
            }
        }
        else if (!strcmp(argv[i], "-s")) // max block size
        {
            if (this->size == UNSET)
            {
                std::string s (argv[i+1]);

                try {
                    this->size = stoi(s);
                } catch (...) {
                    throw std::invalid_argument(ERROR_INVALID_SIZE);
                }

                if (this->size < BLOCK_MINSIZE || BLOCK_MAXSIZE < this->size)
                {
                    throw std::invalid_argument(ERROR_INVALID_SIZE);
                }
            }
            else
            {
                throw std::logic_error(ERROR_DUPLICATE_ARGUMENT);
            }
        }
        else if (!strcmp(argv[i], "-t")) // timeout
        {
            if (this->timeout == UNSET)
            {
                std::string s (argv[i+1]);

                try {
                    this->timeout = stoi(s);

                    if (this->timeout < 1)
                    {
                        throw std::invalid_argument(ERROR_INVALID_TIMEOUT);
                    }
                } catch (...) {
                    throw std::invalid_argument(ERROR_INVALID_TIMEOUT);
                }
            }
            else
            {
                throw std::logic_error(ERROR_DUPLICATE_ARGUMENT);
            }
        }
        else // unknown argument
        {
            throw std::invalid_argument(ERROR_INVALID_ARGUMENT);
        }

    } // end of for cycle of processing of argv

    // did user specify working directory?
    if (!this->directory.size())
    {
        throw std::logic_error(ERROR_MISSING_DIRECTORY);
    }

    // adding closing slash for directory path if missing
    if (this->directory[this->directory.size()-1] != '/')
    {
        this->directory = this->directory + '/';
    }

    // if timeout not set, use default value
    this->timeout = (this->timeout == UNSET)? DEFAULT_ACCEPTED_TIMEOUT:this->timeout;

    // test existence and permissions for working directory
    this->validateWorkingDirectory();

    // process addresses list
    this->processAddresses(address);
}

 /*
 * Parse address-port pairs from address argument. If none is specified, 
 * default address is 127.0.0.1 and default port is 69.
 */
void Arguments::processAddresses(std::string in)
{
    std::stringstream ss(in);
    std::string item;

    if (in.length())
    {
    	while (std::getline(ss, item, ADDRESS_DELIMITER)) // taking addresses one by one
	    {
	        std::string addr = item.substr(0, item.find(ADDR_PORT_DELIMITER)); // parsing address
	        std::string port = item.substr(item.find(ADDR_PORT_DELIMITER)+1, item.length()); // parsing port

	        if (!addr.length()) // if address is empty, use dafult
	        {
	            addr = LOCALHOST;
	        }

	        this->addresses.push_back(addr);

	        if (item.find(ADDR_PORT_DELIMITER) == std::string::npos) //  if port is empty, use default
	        {
	            this->ports.push_back(DEFAULT_PORT);
	        }
	        else // port not empty -> convert to int
	        {
	            this->ports.push_back(port);
	        }
	    }	
    }
    else
    {
    	this->ports.push_back(DEFAULT_PORT);
        this->addresses.push_back(LOCALHOST);
    }
}

/*
 * Prints info about TFTP server setup before it real start.
 */
void Arguments::printArgumentsInfo()
{
    cout(<< "Working directory: " << this->directory << std::endl;)
    cout(<< "Max accepted timeout: " << this->timeout << " second(s)" << std::endl;)
    cout(<< "Max accepted block size: " << this->size << " Bytes" << std::endl << std::endl ;)
}

/*
 * Validates working directory (existence only!)
 */
void Arguments::validateWorkingDirectory()
{
	struct stat sb;

	if (!(stat(this->directory.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)))
	{
		throw std::invalid_argument(ERROR_EXIST_DIRECTORY);
	}
}

/*
 * Getter for working directory.
 */
std::string Arguments::getDirectory()
{
    return this->directory;
}

/*
 * Getter for retransmission timeout time.
 */
unsigned int Arguments::getTimeout()
{
    return this->timeout;
}

/*
 * Getter for block size value.
 */
unsigned int Arguments::getSize()
{
    return this->size;
}

/*
 * Getter for info if user only want help.
 */
bool Arguments::getOnlyHelp()
{
    return this->onlyHelp;
}

/*
 * Getter for specified address
 */
std::string Arguments::getAddress(unsigned int i)
{
	return this->addresses.at(i);
}

/*
 * Getter for specified port
 */
std::string Arguments::getPort(unsigned int i)
{
	return this->ports.at(i);
}

/*
* Getter for number of user pairs interface,port
*/
unsigned int Arguments::getInterfaceCount()
{
    return this->addresses.size();
}
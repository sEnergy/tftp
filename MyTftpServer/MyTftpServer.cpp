/*******************************************************************************
 *
 * Project name:        TFTP server
 * Author:              Marcel Fiala
 * E-mail:              xfiala47@stud.fit.vutbr.cz
 * Date (created):      18th November 2014
 * Encoding:            UTF-8
 *
 ******************************************************************************/

#include "MyTftpServer.h"
#include "Help.h"
#include "Error.h"

#include "atomic_stream.h"

#include <string>
#include <stdexcept>
#include <algorithm>

#include <ctime>
#include <cerrno>
#include <cstring>

#include <iostream>
#include <iomanip>

#include <iostream>
#include <sstream>
#include <fstream>

#include <unistd.h>

#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include <sys/socket.h>
#include <sys/types.h> 
#include <sys/wait.h>
#include <sys/stat.h>

/*
 * Constructor of server, paramaters are arguments of command line (at least one, 
 * working directory, is always required.)
 */
MyTftpServer::MyTftpServer(int argc, char** argv)
{
    this->arguments = new Arguments(argc, argv);

    this->request = NULL;
    this->error = NULL;
}

MyTftpServer::~MyTftpServer ()
{
    delete this->arguments;

    this->packetContents.clear();

    free(this->request);
    free(this->error);
}

/*
 * Either calls Print Help method, or starts calls method that starts service on desired interfaces & ports.
 */
void MyTftpServer::run() 
{
    // user only wants to see help message
    if (this->arguments->getOnlyHelp())
    {
        Help::PrintHelpMessage();
        return;
    }

    /*
     * If control gets here, it is sure user wants tu run in properly, not just for Help.
     */
    this->arguments->printArgumentsInfo();

    cout(<< "Starting services...\n" << std::endl;)
    this->startServices();
}

/*
 * Starts listening service on specified interfaces and ports
 */
void MyTftpServer::startServices()
{
    // get number of forks to make (one for each pair ip,port)
    unsigned int forks = this->arguments->getInterfaceCount();
    
    // make and bind sockets, fork processes for listening
    for (unsigned int i = 0; i < forks; ++i) 
    {
        // get current working ip and port
        std::string p = this->arguments->getPort(i);
        const char* address = this->arguments->getAddress(i).c_str();

        if (!std::all_of(p.begin(), p.end(), ::isdigit) || !p.size() || std::stoi(p) > 65635)
        { 
            cerr(<< "Port '" << p << "' for address " << address << " invalid. Changed to default 69." 
                    << std::endl << std::endl);

            p = DEFAULT_PORT;
        }

        const char* port = p.c_str();

        // socket and socket descriptor
        int sockfd, code;
        struct sockaddr_storage server;
        struct addrinfo hints; // just for getaddrinfo()

        // prepare structures
        memset(&hints, 0, sizeof(hints));
        memset(&server, 0, sizeof(server));

        // set hints for getaddrinfo
        hints.ai_family = AF_UNSPEC; // either IPv4 or IPv6
        hints.ai_flags = AI_NUMERICHOST | AI_NUMERICSERV; // numeric addres, numeric port - both in strings
        hints.ai_socktype = SOCK_DGRAM; // UDP uses datagrams

        if ((code = getaddrinfo(address, port, &hints, &this->result) != 0))
        {
            cerr(<< ERR_GETADDR << address << ":" << port << " - " << gai_strerror(code) << std::endl);
            continue;
        }

        // copy results to server address structure and free result structure
        memcpy(&server, this->result->ai_addr, this->result->ai_addrlen);

        // create UDP socket
        if ((sockfd = socket(this->result->ai_family, this->result->ai_socktype, this->result->ai_protocol)) < 0)
        {
            cerr( << ERROR_DATAGRAM_SOCKET << std::endl;)
            continue;
        } 

        // bind socket to address
        if (bind(sockfd, (struct sockaddr *)&server, sizeof(server)))
        {
            cerr(<< ERROR_BIND << address << ":" << port << " - " << (strerror(errno)) << "." << std::endl;)
            continue;
        }
        
        cout(<<"Started listening on " << address << ":" << port << std::endl;)

        // fork process to listen on given interface and port
        if (!fork())
        {
            this->listen(sockfd);
            return;
        }
    }

    /*
     * Waiting for all children
     */
    for (unsigned int i = 0; i < forks; ++i) 
    {
        wait(NULL);
    }

}

void MyTftpServer::listen(int hostSocket)
{  
    int n;

    this->request = (struct packetComm*) malloc(sizeof(struct packetComm));
    this->error = (struct packetData*) malloc(sizeof(struct packetData));

    this->error->opcode = htons(ERROR);

    while (true) 
    {
        addrLen = sizeof(struct sockaddr);
        this->multicast = this->setBlksize = this->setTimeout = this->setTsize = false;

        this->blksize = DATA_MAXLENGTH;
        this->timeout = DEFAULT_TIMEOUT;

        memset(this->request, 0, sizeof(struct packetComm));
        memset(this->error, 0, sizeof(struct packetData));

        memset(&clientAddr, 0, sizeof(clientAddr));

        this->error->opcode = htons(ERROR);
        
        if ((n = recvfrom(hostSocket, this->request, sizeof(struct packetComm), 0, (struct sockaddr *)&clientAddr, &addrLen)) < 0) 
        {
            cerr(<< ERROR_RECV << errno << std::endl;)
        }
        else
        {
            if (!fork()) // service process
            {
                int clientSocket;

                // create UDP socket
                if ((clientSocket = socket(this->result->ai_family, this->result->ai_socktype, this->result->ai_protocol)) < 0)
                {
                    cerr( << ERROR_DATAGRAM_SOCKET << errno << std::endl;)
                    break;
                }

                struct timeval t;
                t.tv_sec = this->timeout;
                t.tv_usec = 0;

                setsockopt(clientSocket, SOL_SOCKET, SO_RCVTIMEO, &t, sizeof(t));

                if (this->processPacketContents(this->request->contents))
                {
                    cout(<<"blksize = "<< this->blksize << " " << this->setBlksize<< std::endl;)
                    cout(<<"timeout = "<< this->timeout << " " << this->setTimeout<< std::endl;)

                    // if extdended, must negotiate
                    if (this->extended && (this->setTimeout + this->setBlksize+ this->setBlksize))
                    {
                        cout(<<"Negotiate..."<<std::endl;)

                        struct packetComm *neg = (struct packetComm*) malloc(sizeof(struct packetComm));
                        memset(neg, 0, sizeof(struct packetComm));

                        neg->opcode = htons(OACK);

                        char* p = neg->contents; 


                        if (this->setTimeout)
                        {
                            memcpy(p, TIMEOUT, strlen(TIMEOUT));
                            p+=strlen(TIMEOUT)+1;

                            const char *val = std::to_string(this->timeout).c_str();
                            memcpy(p, val, strlen(val));
                            p+=strlen(val)+1;

                        }
                        if (this->setBlksize)
                        {
                            memcpy(p, BLKSIZE, strlen(BLKSIZE));
                            p+=strlen(BLKSIZE)+1;

                            const char *val = std::to_string(this->blksize).c_str();
                            std::cout << "BLOCK "<< val << std::endl;
                            memcpy(p, val, strlen(val));
                            p+=strlen(val)+1;
                        }
                        if (this->setTsize)
                        {
                            memcpy(p, TSIZE, strlen(TSIZE));
                            p+=strlen(TSIZE)+1;

                            const char *val = std::to_string(this->tsize).c_str();
                            memcpy(p, val, strlen(val));
                            p+=strlen(val)+1;    
                        }

                        struct packetData *ack = (struct packetData*) malloc(sizeof(struct packetData));
                        int timeouts = 1;

                        retranstmitOACK: // if recieve of ACK timeouted, send DATA again

                        memset(ack, 0, sizeof(struct packetData));

                        // send packet
                        int code = sendto(clientSocket, neg, 1+p-neg->contents, 0, (struct sockaddr *)&clientAddr, addrLen);

                        if (code == -1) // error
                        {
                            cerr(<< "Error sending OACK packet to" << this->stringIP << ". Transfer terminated." << std::endl;)
                            free(ack);
                            return;
                        }

                        n = recvfrom(clientSocket, ack, sizeof(struct packetComm), 0, NULL, NULL);

                        if (n < 0) // timeout
                        {
                            if (errno == EAGAIN || errno == EWOULDBLOCK) // timeout - retransmit DATA packet
                            {
                                if (timeouts <= 5)
                                {
                                    timeouts++;
                                    goto retranstmitOACK;
                                }
                                else
                                {
                                    cerr(<< "Retransmit limit reached for " << this->stringIP << ". Terminating transfer." << std::endl;)
                                    break;
                                }
                            }
                            else
                            {
                                cerr(<< "Error during recieving ACK packet from " << this->stringIP << ". Terminating transfer." << std::endl;)
                                
                                free(ack);
                                return;
                            }
                        }

                        if (ack->opcode == htons(ERROR))
                        {
                            cerr(<< this->stringIP << " sent ERR packet during negotiation." << std::endl;)
                            
                            free(ack);
                            return;
                        }

                        free(ack);
                    }

                    std::string rq = (this->request->opcode == RRQ)? " READ from ":" WRITE to ";

                    std::string sizeInfo = (this->setTsize)? ", size ": "";

                    if (sizeInfo.size())
                    {
                        sizeInfo = sizeInfo + std::to_string(this->tsize) + " octets";
                    }

                    getnameinfo((struct sockaddr *)&clientAddr, addrLen, this->stringIP, 
                            INET6_ADDRSTRLEN, NULL, 0, NI_NUMERICHOST | NI_NUMERICSERV);

                    cout(<< this->stringIP << " requested" << rq << this->file << sizeInfo << std::endl;)
                    
                    if (this->request->opcode == RRQ)
                    {
                        if (!this->fileExists())
                        {
                            cout(<< this->stringIP << " requested READ of unexistent file - rejected" << std::endl << std::endl;)

                            const char *err = "File not found.";
                            memcpy(this->error->data, err, strlen(err));
                            this->error->block = htons(ERR_FILE_NOT_FOUND); 

                            unsigned int l = 5+strlen(this->error->data);

                            sendto(clientSocket, this->error, l,0, (struct sockaddr *)&clientAddr, addrLen);

                        }
                        else if (!this->fileReadable()) // mam prava?
                        {
                            cout(<< this->stringIP << " requested READ unallowed file - rejected" <<std::endl << std::endl;)

                            const char *err = "Server has insufficient rigths to read specified file.";
                            memcpy(this->error->data, err, strlen(err));
                            this->error->block = htons(ERR_ACCESS_VIOLATION);

                            unsigned int l = 5+strlen(this->error->data);
                            sendto(clientSocket, this->error, l,0, (struct sockaddr *)&clientAddr, addrLen);
                        }
                        else
                        {
                            this->send(clientSocket);
                        }
                    }
                    else // WRQ
                    {
                        if (this->fileExists())
                        {
                            cout(<< this->stringIP << " requested WRITE to existing file - rejected" <<std::endl << std::endl;)

                            this->error->block = htons(ERR_FILE_ALREADY_EXISTS);
                            const char *err = "File already exists on server.";
                            memcpy(this->error->data, err, strlen(err));

                            unsigned int l = 5+strlen(this->error->data);
                            sendto(clientSocket, this->error, l, 0, (struct sockaddr *)&clientAddr, addrLen);
                        }
                        else
                        {
                            this->recieve(clientSocket);
                        }
                    }

                }
                else // error
                {
                    this->error->block = htons(this->error->block);

                    unsigned int l = 5+strlen(this->error->data);
                    sendto(clientSocket, this->error, l,0, (struct sockaddr *)&clientAddr, addrLen);
                }
            
                return;
            } 
        }
    }
}

void MyTftpServer::send(int clientSocket)
{
    int timeouts = 1;

    FILE* file;
    unsigned int number = 1;

    struct packetData *ack = (struct packetData*) malloc(sizeof(struct packetData));
    struct packetData *data = (struct packetData*) malloc(sizeof(struct packetData));

    memset(ack, 0, sizeof(struct packetData));
    memset(data, 0, sizeof(struct packetData));

    data->opcode = htons(DATA);

    if ((file = fopen(this->file.c_str(), "rb")) == NULL) 
    {
        cerr(<< "Error during opening file " << this->file.c_str() << ". Transfer to " << this->stringIP << "terminated." << std::endl;)

        const char *err = "Error during opening file.";
        memcpy(ack->data, err, strlen(err));
        ack->block = htons(ERR_UNDEFINED); 

        unsigned int l = 5+strlen(this->error->data);

        sendto(clientSocket, ack, l, 0, (struct sockaddr *)&clientAddr, addrLen);

        free(ack);
        free(data);

        return;
    }

    cout(<< "Started sending " << this->file << " to " << this->stringIP << std::endl;)

    while (42)
    {
        data->block = htons(number);

        int n = fread(data->data, 1, this->blksize, file);
        cerr(<<"fread "<<n<<std::endl;)

        if (!n) // EOF
        {
            if (feof(file))
            {
                cout(<< this->file << " successfully sent to " << this->stringIP << std::endl << std::endl;)
                break; 
            }
            
            cerr(<<"chyba";)
        }

        retranstmit: // if recieve of ACK timeouted, send DATA again

        // send packet
        int code = sendto(clientSocket, data, 4+n, 0, (struct sockaddr *)&clientAddr, addrLen);
        cerr(<<"sent packet  "<<number<<std::endl;)

        if (code == -1) // error
        {
            cerr(<< "Error sending packet to" << this->stringIP << ". Transfer terminated." << std::endl;)
            break;
        }

        n = recvfrom(clientSocket, ack, sizeof(struct packetComm), 0, NULL, NULL);
        cerr(<<"recieved packet size "<<n<<std::endl;)

        if (n < 0) // timeout
        {
            if (errno == EAGAIN || errno == EWOULDBLOCK) // timeout - retransmit DATA packet
            {
                if (timeouts <= 5)
                {
                    timeouts++;
                    cout(<< "Timeout limit reached for " << this->stringIP << ". Retransmitting." << std::endl;)
                    goto retranstmit;
                }
                else
                {
                    cerr(<< "Retransmit limit reached for " << this->stringIP << ". Terminating transfer." << std::endl;)
                    break;
                }
            }
            else
            {
                cerr(<< "Error during recieving ACK packet from " << this->stringIP << ". Terminating transfer." << std::endl;)
                break;
            }
        }

        if (ack->opcode == htons(ERROR))
        {
            cerr(<< this->stringIP << "sent ERR packet. Terminating transfer." << std::endl;)
            break;
        }

        number++;
    }

    fclose(file);
    
    free(ack);
    free(data);
}

void MyTftpServer::recieve(int clientSocket)
{
    int n, timeouts = 1;

    FILE* file;
    unsigned int number = 1;

    struct packetData *ack = (struct packetData*) malloc(sizeof(struct packetData));
    struct packetData *data = (struct packetData*) malloc(sizeof(struct packetData));

    memset(ack, 0, sizeof(struct packetData));
    memset(data, 0, sizeof(struct packetData));

    ack->opcode = htons(ACK);
    ack->block = htons(0);

    // trying to open a file
    if ((file = fopen(this->file.c_str(), "wb")) == NULL) 
    {
        cerr(<< "Error during creating file " << this->file.c_str() << ". Transfer terminated." << std::endl;)

        const char *err = "Error during creating a file.";
        memcpy(ack->data, err, strlen(err));
        ack->block = htons(ERR_UNDEFINED); 

        unsigned int l = 5+strlen(this->error->data);

        sendto(clientSocket, ack, l, 0, (struct sockaddr *)&clientAddr, addrLen);

        free(ack);
        free(data);

        return;
    }
    else
    {
        // sending ACK - ready, send me the file
        sendto(clientSocket, ack, 4, 0, (struct sockaddr *)&clientAddr, addrLen);

        cout(<< this->stringIP << " started writing to " << this->file << std::endl;)
    }

    while (true)
    {
        recieveNewPacket:

        ack->opcode = htons(ACK);
        ack->block = htons(number); // save new expected number

        // recieve data from client
        n = recvfrom(clientSocket, data, sizeof(struct packetData), 0, NULL, NULL);

        if (n < 0)
        {
            if (errno == EAGAIN || errno == EWOULDBLOCK) // timeout - send ACK for last packet again
            {
                if (timeouts <= 5)
                {
                    timeouts++;
                    cout(<< "Timeout limit reached for " << this->stringIP << ". Retransmitting." << std::endl;)

                    ack->block = htons(number-1);
                    sendto(clientSocket, ack, 4, 0, (struct sockaddr *)&clientAddr, addrLen);

                    goto recieveNewPacket;
                }
                else
                {
                    cout(<< "Retransmit limit reached for " << this->stringIP << ". Terminating transfer." << std::endl;)
                    break;
                }
            }
            else
            {
                cerr(<< "Error during recieving DATA packet from " << this->stringIP << ". Terminating transfer." << std::endl;)
                break;
            }

        }

        if (data->opcode == htons(ERROR))
        {
            cerr(<< "Recieved error packet - code " << ntohs(data->opcode) << ". Transfer terminated." << std::endl;)
            break;
        }

        // ack for packet
        sendto(clientSocket, ack, 4, 0, (struct sockaddr *)&clientAddr, addrLen);

        // correct packet -> save data to file
        if (n == 4+this->blksize)
        {   
            fwrite(data->data, 1, this->blksize, file);
        }
        else
        {
            fwrite(data->data, 1, n-4, file);

            cerr(<< this->stringIP << " successfully wrote to " << this->file.c_str() << std::endl << std::endl;)

            break;
        }

        number++; // set new expected number
    }

    free(ack);   
    free(data);

    fclose(file);
}

/*
 * Processes contents of request packet
 */
int MyTftpServer::processPacketContents(char raw[CONTENTS_MAXLENGTH])
{
    // translate opcode
    this->request->opcode = ntohs(this->request->opcode);

    // only RRQ or WRG allowed
    if (this->request->opcode > WRQ || this->request->opcode <= 0)
    {
        this->error->block = ERR_ILLEGAL_OP;
        return RQ_ERR;
    }

    // parse request packet contents
    if (!this->parsePacketContents(raw))
    {
        return RQ_ERR;
    }

    if (!validateFileName())
    {
        return RQ_ERR;
    }

    // save complete path to file
    this->file = this->arguments->getDirectory();
    this->file += this->packetContents.at(0);

    // use request packet contents for settings
    return this->usePacketContents();
}

/*
 * Parses contents of request packet
 */
int MyTftpServer::parsePacketContents(char raw[CONTENTS_MAXLENGTH])
{
    this->packetContents.clear();
    unsigned int l = 0;

    // split data by delimiter '\0'
    while((l = strlen(raw)))
    {
       this->packetContents.push_back(raw);
       raw += l+1; 
    }

    unsigned int s = this->packetContents.size();

    // due to case insensitiity, transform all characters to lower case
    std::string mode = this->packetContents.at(1);
    std::transform(mode.begin(), mode.end(), mode.begin(), ::tolower);

    if (s < 2) // at least filename and mode required
    {
        this->error->block = ERR_UNDEFINED;

        const char *err = "Filename and/or mode missing.";
        memcpy(this->error->data, err, strlen(err));

        return RQ_ERR;
    }

    // reject multicast
    if (mode == MULTICAST)
    {
        this->error->block = ERR_UNDEFINED;

        const char *err = "Multicast unsupportted.";
        memcpy(this->error->data, err, strlen(err));

        return RQ_ERR;
    }

    if (mode == NETASCII || mode == OCTET)
    {
        if (!(s%2)) // must be even (option-value pairs)
        {
            this->mode = mode;
            this->extended = (s > 2);

            return RQ_OK;
        }
        else
        {
            this->error->block = ERR_UNDEFINED;

            const char *err = "Uneven number of options and values.";
            memcpy(this->error->data, err, strlen(err));

            return RQ_ERR;
        }
    }
    else // mail mode not supported
    {
        this->error->block = ERR_UNDEFINED;

        const char *err = "Unsupported mode requested.";
        memcpy(this->error->data, err, strlen(err));

        return RQ_ERR;
    }
}

/*
 * Sets server settings and responses packet contents according to values from request packet
 */
int MyTftpServer::usePacketContents()
{
    // iterate for each pair option-value
    for (unsigned int i = 2; this->packetContents.size()-1 >= i; i+=2)
    {
        // due to case insensitiity, transform all characters to lower case
        std::string param = this->packetContents.at(i);
        std::transform(param.begin(), param.end(), param.begin(), ::tolower);

        std::string value = this->packetContents.at(i+1);

        if (param == BLKSIZE)
        {
            //// jeste doladit s mtu !!!!
            this->blksize = std::stoi(value);

            if (this->blksize > BLOCK_MINSIZE || BLOCK_MAXSIZE < this->blksize)
            {
                if (this->blksize > this->arguments->getSize())
                {
                    this->blksize = this->arguments->getSize();
                }

                this->setBlksize = true;

            }
            else
            {
                this->blksize = DATA_MAXLENGTH;
            }

        }
        else if (param == TIMEOUT)
        {
            this->timeout = std::stoi(value);

            if (this->timeout <= this->arguments->getTimeout())
            {
                this->setTimeout = true;
            }
            else
            {
                this->timeout = DEFAULT_TIMEOUT;
            }

        }
        else if (param == TSIZE)
        {
            this->setTsize = true;

            if (this->request->opcode == WRQ) 
            {
                this->tsize = std::stoi(value);
            }
            else
            {
                if (this->fileExists())
                {
                    if (fileReadable())
                    {
                        this->tsize = getFilesize();
                    }
                    else
                    {
                        this->error->block = ERR_ACCESS_VIOLATION;

                        const char *err = "Server has insufficient permissions to read the file.";
                        memcpy(this->error->data, err, strlen(err));

                        return RQ_ERR;
                    }
                }
                else
                {
                    this->error->block = ERR_FILE_NOT_FOUND;

                    const char *err = "File not found on server.";
                    memcpy(this->error->data, err, strlen(err));

                    return RQ_ERR;
                }
            }
        }


    }

    return RQ_OK;
}

/*
 * Validates filename (length and presence of slash).
 *
 * Returns nonzero values on success.
 */
int MyTftpServer::validateFileName()
{
    char *filename = this->packetContents.at(0);

    // filename must not contain slash
    if (strchr(filename,'/') != NULL)
    {
        this->error->block = ERR_UNDEFINED;

        const char *err = "Filename consists '/' character.";
        memcpy(this->error->data, err, strlen(err));

        return RQ_ERR;
    }

    // length must be be less or equal to FILENAME_MAXLENGTH
    if (strlen(filename) > FILENAME_MAXLENGTH)
    {
        this->error->block = ERR_UNDEFINED;

        const char *err = "Filename longer than 255 characters.";
        memcpy(this->error->data, err, strlen(err));

        return RQ_ERR;
    }

    return RQ_OK;
}

/*
 * Returns bool value about existence of file specified by working directory and filename
 */
bool MyTftpServer::fileExists() 
{
    struct stat x;
    return (stat(this->file.c_str(), &x) == 0);   
}

/*
 * Returns bool value about readability of file specified by working directory and filename
 */
bool MyTftpServer::fileReadable ()
{
  std::ifstream ifile(this->file);
  return ifile;
}

size_t MyTftpServer::getFilesize() 
{
    struct stat result;

    if (stat(this->file.c_str(), &result) != 0) 
    {
        return 0;
    }

    return result.st_size;   
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 /*               struct ifaddrs *ifaddr, *ifa;
               int  n;

               if (getifaddrs(&ifaddr) == -1) {
                   perror("getifaddrs");
                   exit(EXIT_FAILURE);
               }

               int rc = getsockname(server_sock,(struct sockaddr *)&clientAddr, &addrLen);


               for (ifa = ifaddr, n = 0; ifa != NULL; ifa = ifa->ifa_next, n++) {
                   if (ifa->ifa_addr == NULL || ifa->ifa_addr != rc)
                       

                   std::cout << ifa->ifa_name <<std::endl;;
               }


*/
////////////////////////////////////////////////////////////////////////////////////////////////////////////////